package com.zuitt.batch193;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
    Scanner appScanner = new Scanner(System.in);

            System.out.println("Input year to be checked if it is a leap year.");
            int leapYear = appScanner.nextInt();

            if (leapYear % 4 == 0) {
                System.out.println(leapYear + " " + "is a leap Year.");
            } else if(leapYear % 100 == 0){
                System.out.println(leapYear + " " + "is not a leap year.");
            } else if(leapYear % 400 == 0) {
                System.out.println(leapYear + " " + "is a leap Year.");
            } else {
                System.out.println(leapYear + " " + "is not a leap year.");
            }

        }
}
