package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class WDC042_S2_A2 {
    public static void main(String[] args){
        int[] primeArray = new int[5];
        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        Scanner primeScanner = new Scanner(System.in);
        System.out.println("The first prime number is: " + primeArray[0]);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Gary");
        friends.add("Squidward");
        friends.add("Patrick");
        friends.add("Sandy");
        System.out.println("Spongebob's friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("Toothpaste", 78);
        inventory.put("Toothbrush", 77);
        inventory.put("Soap", 72);
        System.out.println("Our current inventory consists of: " + inventory);



    }

}
